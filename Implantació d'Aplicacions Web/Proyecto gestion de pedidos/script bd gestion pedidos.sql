CREATE TABLE tblcliente
( 
    idCliente INT PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    strDni VARCHAR(16) NOT NULL , 
    strNombre VARCHAR(50) NOT NULL ,
    strApellidos VARCHAR(100) NOT NULL ,
    strDireccion VARCHAR(100) NOT NULL ,
    strLocalidad VARCHAR(50) NOT NULL , 
    strProvincia VARCHAR(50) NOT NULL , 
    intCp INT NOT NULL,
)
;